<?php

namespace App\Contract\Repository;
use App\Models\Task;
use App\Models\User;

interface CustomTaskModelInterface
{
    public function index();

    public function storeTask(array $data);

    public function getAllUsersTask();

    public function updateTask(Task $task,array $data);

    public function mentionAdminInTask(Task $task);

    public function deleteTask(Task $task);

    public function checkWhoCreatedTaskThenDelete(Task $task);
}
