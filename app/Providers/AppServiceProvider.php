<?php

namespace App\Providers;

use App\Contract\Services\CustomTaskServiceInterface;
use App\Contract\Services\CustomUserServiceInterface;
use App\Services\CustomTaskService;
use App\Services\CustomUserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CustomUserServiceInterface::class, CustomUserService::class);
        $this->app->bind(CustomTaskServiceInterface::class, CustomTaskService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
