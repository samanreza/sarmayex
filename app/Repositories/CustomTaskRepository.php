<?php

namespace App\Repositories;
use \App\Models\Task;
use App\Models\User;
use \App\Models\UserTask;
use App\Contract\Repository\CustomTaskModelInterface;
use Illuminate\Support\Facades\DB;

class CustomTaskRepository implements CustomTaskModelInterface
{
    public function index(): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        return Task::query()->select([
            Task::COLUMN_ID,
            Task::COLUMN_TITLE,
            Task::COLUMN_DESCRIPTION,
            Task::COLUMN_CREATED_AT,
            Task::COLUMN_UPDATED_AT
        ])->paginate();
    }



    public function getAllUsersTask()
    {
        return User::query()->find(auth('api')->user()['id'])->taskUser;
    }

    public function storeTask(array $data)
    {
        try {
            DB::beginTransaction();
                $newTask = new Task();
                $newTask->setTitle($data[Task::COLUMN_TITLE]);
                $newTask->setDescription($data[Task::COLUMN_DESCRIPTION]);
                $newTask->save();

                $newTask->userTasks()->attach($newTask,[
                    UserTask::COLUMN_USER_ID => auth('api')->user()['id']
                ]);

            DB::commit();
                return $newTask;
        }catch (\Exception $exception){
            DB::rollBack();
        }
    }

    public function updateTask(Task $task,$data)
    {
        try {
            DB::beginTransaction();
                $task->setTitle($data[Task::COLUMN_TITLE])

                    ->setDescription($data[Task::COLUMN_DESCRIPTION])

                    ->update();

                $task->userTasks()->updateExistingPivot($task,[
                    UserTask::COLUMN_TASK_ID => $task->getId(),
                ]);
            DB::commit();
                return $task;
        }catch (\Exception $exception){
            DB::rollBack();
            dd($exception);
        }
    }

    public function mentionAdminInTask(Task $task)
    {
        DB::transaction(function () use($task){
           $task->userTasks()->attach($task,[
               UserTask::COLUMN_USER_ID => auth('api')->user()['id']
           ]);
        });
    }

    public function deleteTask(Task $task)
    {
        DB::transaction(function () use($task){

            $task->userTasks()->detach();

            $task->delete();
        });
    }

    public function checkWhoCreatedTaskThenDelete(Task $task)
    {
        $data = $this->getAllUsersTask();
        return $data->pluck('id')->toArray();
    }
}
