<?php

namespace App\Http\Controllers;

use App\Contract\Services\CustomTaskServiceInterface;
use App\Http\Requests\TaskRequest;
use App\Http\Resources\TaskResource;
use App\Models\Task;
use App\Models\User;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    private CustomTaskServiceInterface $customTaskService;

    public function __construct(CustomTaskServiceInterface $customTaskService)
    {
       $this->customTaskService = $customTaskService;
    }

    public function index()
    {
        $list = $this->customTaskService->index();

        return TaskResource::collection($list);
    }

    /**
     * @param TaskRequest $taskRequest
     * @return TaskResource
     */
    public function store(TaskRequest $taskRequest): TaskResource
    {
        $data = $this->_datasizeData($taskRequest);

        return new TaskResource($this->customTaskService->storeTask($data));
       /* return response()->json([
            'data' => $this->customTaskService->storeTask($data)
        ],Response::HTTP_CREATED);*/
    }

    /**
     * @param TaskRequest $taskRequest
     * @param Task $task
     * @return TaskResource
     */
    public function update(TaskRequest $taskRequest,Task $task): TaskResource
    {
        $data = $this->_datasizeData($taskRequest);

        return new TaskResource($this->customTaskService->updateTask($task,$data));
        /*return response()->json([
            'data' => $this->customTaskService->updateTask($task,$data)
        ]);*/
    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Task $task): \Illuminate\Http\JsonResponse
    {
        $this->customTaskService->deleteTask($task);

        return response()->json(null, Response::HTTP_NO_CONTENT);

    }

    /**
     * @param Task $task
     * @return \Illuminate\Http\JsonResponse
     */
    public function mentionInTask(Task $task): \Illuminate\Http\JsonResponse
    {
         $this->customTaskService->mentionTask($task);

         return \response()->json(['message' => 'you succeeded mention on that task']);
    }

    public function getTaskListByEachUser()
    {
        return $this->customTaskService->getTaskListByUser();
    }

    /**
     * @param TaskRequest $taskRequest
     * @return array
     */
    private function _datasizeData(TaskRequest $taskRequest): array
    {
        return [
           Task::COLUMN_TITLE => $taskRequest->{Task::COLUMN_TITLE},
           Task::COLUMN_DESCRIPTION =>$taskRequest->{Task::COLUMN_DESCRIPTION},
        ];
    }
}
