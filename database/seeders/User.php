<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::query()->create([
            'username' => 'sami',
            'password' => bcrypt('123456'),
            'role' => 'member'
        ]);

        \App\Models\User::query()->create([
            'username' => 'saman',
            'password' => bcrypt('123456'),
            'role' => 'admin'
        ]);
    }
}
